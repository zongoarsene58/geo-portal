import React from 'react';
import { useDispatch, useSelector } from 'react-redux'
import {showMainMenu} from "../../Redux"
import { Link } from "react-router-dom";
import { Container, Row, Col } from 'react-bootstrap';

export const Header = () => {

    const dispatch = useDispatch()

    const openMenu = async (e) => {
        e.preventDefault()
        dispatch(showMainMenu())
    }

    return (
        <div className="header">
            <Container fluid>
                <Row>
                    <Col className="menu-h" lg="1">
                        <div className="py-3" onClick={openMenu}>
                            MENU
                        </div>
                    </Col>
                    <Col>
                        
                    </Col>
                </Row>
            </Container>
        </div>
    )
}

export const Footer = () => {
    return (
        <div className="footer">
            <Container>
                <Row>
                    <Col>
                        Footer
                    </Col>
                </Row>
            </Container>            
        </div>
    )
}

const Main = ({ children }) => {
    
    const app = useSelector(state => state.app)

    return (
        <div className="main">
            <Container fluid>
                <Row>
                    {app.show_main_menu && 
                    <Col className="main-menu" lg="2">
                        <div className="">
                            <div>
                                <Link className="link" to="/" >Home</Link>
                            </div>
                            <div>
                                <Link className="link" to="/carte">Carte</Link>
                            </div>
                        </div>
                    </Col>}
                    <Col className="main-content">
                        {children}
                    </Col>
                </Row>
            </Container>         
        </div>
    )
}


export const Layout = ({children}) => {

    return (
        <div className="body">
            <Header />
                <Main>
                    {children}
                </Main>
            <Footer/>
        </div>
    );
};

export default Layout;


