import React from 'react'
import { MapContainer, TileLayer, Marker, Popup} from 'react-leaflet'
import { getIcon } from '../../Utils'


const pos = {
    lng: 5.3510144,
    lat: -3.997696
}


const Map = () => {
    return (
        <MapContainer center={[pos.lng, pos.lat]} zoom={13} scrollWheelZoom={true}>
            <TileLayer
                attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
            />
            <Marker position={[pos.lng, pos.lat]} icon={getIcon(50)}>
                <Popup>
                    A pretty CSS3 popup. <br /> Easily customizable.
                </Popup>
            </Marker>
        </MapContainer>
    )
}

export default Map
