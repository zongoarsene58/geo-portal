import React from 'react';
import { Map, Layout} from '../Components'


const MapPage = () => {

    return (
        <Layout>
            <Map />
        </Layout>
    );
};

export default MapPage;