import {SHOW_MAIN_MENU} from "../../Redux"

export const showMainMenu = () => async (dispatch) => {
    await dispatch({type: SHOW_MAIN_MENU})
}