import {SHOW_MAIN_MENU} from "../../Redux"

export const App = (state = { app: true, show_main_menu: false }, action) => {
    const { type, payload } = action
    
    switch (type) {
        case SHOW_MAIN_MENU:
            return {...state, show_main_menu: !state.show_main_menu}
        default:
            return state
    }
}
