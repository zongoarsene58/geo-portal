import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useRouteMatch,
  useParams
} from "react-router-dom";
import {Home, Map} from "../Pages";


const MainRouter = () => {

    return (
        <Router>
            <Switch>
                <Route path="/carte" component={ Map } />
                <Route path="/" component={ Home } />
            </Switch>
        </Router>
    );
};

export default MainRouter;