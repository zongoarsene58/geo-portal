import Leaflet  from 'leaflet'

export const getIcon = (_iconSize) => {
    return Leaflet.icon({
        iconUrl: "/images/picker.svg",
        iconSize: _iconSize
    })
}